var test = require('tape');
var filter = require('../filterXML');


test('should filter propperly',function (t) {
	
	test('collection should be properly filtered', function (assert) {
			
			var asd = {
				component : '',
				assignable: true,
				media_type : '',
				component_type : '',
				categorization : ''
			}
			var res = filter.collectionFilter([{
				component : 'Level Up Tutorials',
				assignable: true,
				media_type : 'url',
				component_type : 'online activity',
				categorization : 'Interactive Activities'
			}, 
			{
				component : 'Level Up Tutorials',
				assignable: false,
				media_type : 'url',
				component_type : 'online activity',
				categorization : 'Interactive Activities'
			}]);		
			assert.equal(1, res.length);
			assert.end();
	});

	

	t.end();
});
