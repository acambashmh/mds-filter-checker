var flattenXMLResource = require('./flattenXmlResource');
var loadXMLMetadata = require('./loadXMLMetadata');
var filterXMLResources = require('./filterXML');
var fs = require('fs');
var flattenToc = require('./flattenToc');
var _ = require('lodash');

function getIdList(resources) {
	var result = [];
	resources.forEach(function (resource) {
		if (resource && resource.HMH_ID) {
			result.push(resource.HMH_ID);
		}
	});
	return result;
}

function getCollections(xmlPath, jsonPath) {
	return loadXMLMetadata.loadFromLocal([xmlPath], flattenXMLResource)
		.then(function (xmlResources) {
			return filterXMLResources.collectionFilter(xmlResources);
		})
		.then(function (filteredXmlResources) {
			var tocData = require(jsonPath);
			var flattenTocRes = flattenToc(tocData).map(function (resource) {
				return {
					HMH_ID: resource.HMH_ID,
					component: resource.component,
					assignable: resource.assignable,
					media_type: resource.media_type,
					component_type: resource.component_type,
					categorization: resource.categorization,
				};
			});



			filteredXmlResources = _.sortBy(filteredXmlResources, 'HMH_ID');
			flattenTocRes = _.sortBy(flattenTocRes, 'HMH_ID');
			
			console.log(filteredXmlResources.length)
			console.log(flattenTocRes.length)
			
			var notInXmlResources = _.difference(getIdList(flattenTocRes), getIdList(filteredXmlResources));
			var notInTocResources = _.difference(getIdList(filteredXmlResources), getIdList(flattenTocRes));
				
			fs.writeFileSync('notInXmlResources.json', JSON.stringify(notInXmlResources, null, 4));
			fs.writeFileSync('notInTocResources.json', JSON.stringify(notInTocResources, null, 4));
			fs.writeFileSync('fromXML.json', JSON.stringify(filteredXmlResources, null, 4));
			fs.writeFileSync('fromToc.json', JSON.stringify(flattenTocRes, null, 4));
		}).
		catch(function (err) {
			console.log(err);
		})
}


getCollections('./ca-collection-grade----8.xml', './ca-collection-grade-8-toc.json')