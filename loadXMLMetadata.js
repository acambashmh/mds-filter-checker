var Xml2object = require('xml2object');
var q = require('bluebird-q');
var Promise = require('bluebird');
var request = require('request');
var fs = require('fs');
var pump = require('pump');

function factoryXml2object(xmlNodeToExtract) {
	return new Xml2object([xmlNodeToExtract]);
}

function loadAndConvert(metadataXmlStream, convertResourceToFlatJson) {
	var defer = q.defer();
	var resources = [];
	var parser = factoryXml2object('resource');
	var saxStream = parser.saxStream;
	pump(metadataXmlStream, saxStream, function(err) {
		console.log('pipe finished', err)
	});
	
	metadataXmlStream.on('response', function (response) {
		if (response.statusCode !== 200) {
			defer.reject('error with http request for xml');
		}
	})

	metadataXmlStream.on('error', function (err) {
		defer.reject(err);
	})

	parser.on('object', function (name, obj) {
		if (obj.HMH_ID) {
			resources.push(convertResourceToFlatJson(obj));
		}
	});

	parser.on('error', function (err) {
		defer.resolve('XML parsing error ' + JSON.stringify(err, null, 2));
	});

	parser.on('end', function () {
		defer.resolve(resources)
	});

	return defer.promise;
}



function loadFromSnv(svnUrlPath, username, password, convertResourceToFlatJson) {
	return q.fcall(function () {
		return request.get(svnUrlPath, {
			'auth': {
				'user': username,
				'pass': password,
				'sendImmediately': false
			}
		});
	}).then(function (metadataXmlStream) {
		return loadAndConvert(metadataXmlStream, convertResourceToFlatJson);
	})
}

function loadFromLocal(localPath, convertResourceToFlatJson) {
	return q.fcall(function () {
		return fs.createReadStream(localPath, 'utf-8');
	}).then(function (metadataXmlStream) {
		return loadAndConvert(metadataXmlStream, convertResourceToFlatJson);
	})
}


function loadMultipleFromSvn(svnUrlPaths, username, password, convertResourceToFlatJson) {
	return q.fcall(function () {
		if (!Array.isArray(svnUrlPaths) || svnUrlPaths.length === 0) {
			throw 'path array provided is not correct';
		}
		return svnUrlPaths
	}).then(function (svnUrlPaths) {

		return q.all(svnUrlPaths.map(function (svnUrlPath) {
			return loadFromSnv(svnUrlPath, username, password, convertResourceToFlatJson)
		}))
	}).then(function (resourceGroups) {
		return resourceGroups.reduce(function (prev, curr) {
			return prev.concat(curr);
		});
	});
}

function loadMultipleFromLocal(localPaths, convertResourceToFlatJson) {
	return q.fcall(function () {
		if (!Array.isArray(localPaths) || localPaths.length === 0) {
			throw 'path array provided is not correct';
		}
	}).then(function () {
		return q.all(localPaths.map(function (svnUrlPath) {
			return loadFromLocal(svnUrlPath, convertResourceToFlatJson)
		}))
	}).then(function (resourceGroups) {
		return resourceGroups.reduce(function (prev, curr) {
			return prev.concat(curr);
		});
	});
}

module.exports.loadFromSvn = loadMultipleFromSvn;
module.exports.loadFromLocal = loadMultipleFromLocal;
