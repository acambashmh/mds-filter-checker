function collectionFilter(resources, gradeResOnly) {
	var result = [];
	resources.forEach(function (resource) {
		var test = true;
		
		if(gradeResOnly){
			if(resource.L2){
				test = false;
			}else{
				test = true;
			}
		}else{
			if(resource.L2){
				test = true;
			}else{
				test = false;
			}
		}
		
		resource = {
			HMH_ID : resource.HMH_ID,
			component : resource.component,
			assignable : resource.assignable,
			media_type : resource.media_type,
			component_type : resource.component_type,
			categorization : resource.categorization,
		}
		
		// if(resource.HMH_ID === 'LTCA17_G08C01L06_MW_004'){
		// 	console.log(resource);
		// }
		
		if(resource.component === 'Level Up Tutorials' && test
		&& resource.assignable 
		&& resource.media_type === 'url'
		&& resource.component_type === 'online activity'
		&& resource.categorization === 'Interactive Activities'){
			result.push(resource);
		}
		
		else if(resource.component === 'Student Edition' && test
		&& resource.assignable 
		&& resource.media_type === 'url'
		&& resource.component_type === 'SE'
		&& resource.categorization === 'Core Components'){
			result.push(resource);
		}
		
		else if(resource.component === 'A&E Biography Videos' && test
		&& resource.assignable 
		&& resource.media_type === 'url'
		&& resource.component_type === 'movie'
		&& resource.categorization === 'Video & Audio'){
			result.push(resource);
		}
		
		else if(resource.component === 'History Videos' && test
		&& resource.assignable 
		&& resource.media_type === 'url'
		&& resource.component_type === 'movie'
		&& resource.categorization === 'Video & Audio'){
			result.push(resource);
		}
		
		else if(resource.component === 'Additional Texts by Collection' && test
		&& resource.assignable 
		&& resource.media_type === 'pdf'
		&& resource.component_type === 'ancillary'
		&& resource.categorization === 'Teaching Aids'){
			result.push(resource);
		}
		
		else if(resource.component === 'Writing/Speaking and Listening Collections' && test
		&& resource.assignable
		&& resource.media_type === 'url'
		&& resource.component_type === 'SE'
		&& resource.categorization === 'Core Components'){
			result.push(resource);
		}
		
		else if(resource.component === 'Close Reader' && test
		&& resource.assignable 
		&& resource.media_type === 'url'
		&& resource.component_type === 'SE'
		&& resource.categorization === 'Core Components'){
			result.push(resource);
		}
		
		else if(resource.component === 'Language Workshop' && test
		&& resource.assignable 
		&& resource.media_type === 'pdf'
		&& resource.component_type === 'ancillary'
		&& resource.categorization === 'Teaching Aids'){
			result.push(resource);
		}
		
		else if(resource.component === 'Close Read Applications' && test
		&& resource.assignable
		&& resource.media_type === 'pdf'
		&& resource.component_type === 'ancillary'
		&& resource.categorization === 'Teaching Aids'){
			result.push(resource);
		}
		
		else if(resource.component === 'Common Core Assessment' && test
		&& resource.assignable
		&& resource.media_type === 'pdf'
		&& resource.component_type === 'ancillary'
		&& resource.categorization === 'Teaching Aids'){
			result.push(resource);
		}
		
		else if(resource.component === 'myWriteSmart' && test
		&& resource.assignable 
		&& resource.media_type === 'url'
		&& resource.component_type === 'ancillary'
		&& resource.categorization === 'Interactive Activities'){
			result.push(resource);
		}
		
		else if(resource.component === 'FYI Site' && test
		&& resource.assignable 
		&& resource.media_type === 'url'
		&& resource.component_type === 'ancillary'
		&& resource.categorization === 'Teaching Aids'){
			result.push(resource);
		}
		
		else if(resource.component === 'Additional Texts by Collection' && test
		&& resource.assignable
		&& resource.media_type === 'pdf'
		&& resource.component_type === 'ancillary'
		&& resource.categorization === 'Teaching Aids'){
			result.push(resource);
		}
		
		else if(resource.component === 'Stream to Start Videos' && test
		&& resource.assignable
		&& resource.media_type === 'url'
		&& resource.component_type === 'ancillary'
		&& resource.categorization === 'Teaching Aids'){
			result.push(resource);
		}
		
		else if(resource.component === 'Close Read Screencasts' && test
		&& resource.assignable 
		&& resource.media_type === 'url'
		&& resource.component_type === 'movie'
		&& resource.categorization === 'Video & Audio'){
			result.push(resource);
		}
		
		else if(resource.component === 'Text in Focus Videos' && test
		&& resource.assignable
		&& resource.media_type === 'url'
		&& resource.component_type === 'ancillary'
		&& resource.categorization === 'Teaching Aids'){
			result.push(resource);
		}
		
		else if(resource.component === 'Videos for Media Lessons' && test
		&& resource.assignable 
		&& resource.media_type === 'Audio'
		&& resource.component_type === 'Audio'
		&& resource.categorization === 'Audio and Video'){
			result.push(resource);
		}
		
	});
	return result;
}

function escalateFilter(resources, gradeResOnly) {
	var result = [];
	resources.forEach(function (resource) {
		var test = true;
		
		if(gradeResOnly){
			if(resource.L2){
				test = false;
			}else{
				test = true;
			}
		}else{
			if(resource.L2){
				test = true;
			}else{
				test = false;
			}
		}
		
		resource = {
			HMH_ID : resource.HMH_ID,
			component : resource.component,
			assignable : resource.assignable,
			media_type : resource.media_type,
			component_type : resource.component_type,
			categorization : resource.categorization,
		}
		
		if(resource.component === 'Podcasts'  && test
		&& resource.assignable 
		&& resource.media_type === 'Audio'
		&& resource.component_type === 'Audio'
		&& resource.categorization === 'Audio and Video'){
			result.push(resource);
		}
		
		else if(resource.component === 'Browse Magazine'  && test
		&& resource.assignable 
		&& resource.media_type === 'HTML'
		&& resource.component_type === 'Key Student Resource'
		&& resource.categorization === 'Core Components'){
			result.push(resource);
		}
		
		else if(resource.component === 'Student Edition'  && test
		&& resource.assignable 
		&& resource.media_type === 'HTML'
		&& resource.component_type === 'Key Student Resource'
		&& resource.categorization === 'Core Components'){
			result.push(resource);
		}
		
		else if(resource.component === 'Student Activity Book'  && test
		&& resource.assignable 
		&& resource.media_type === 'PDF'
		&& resource.component_type === 'Ancillary'
		&& resource.categorization === 'Study Aids and Workbooks'){
			result.push(resource);
		}
		
		else if(resource.component === 'Student Writing Models'  && test
		&& resource.assignable 
		&& resource.media_type === 'PDF'
		&& resource.component_type === 'Ancillary'
		&& resource.categorization === 'Study Aids and Workbooks'){
			result.push(resource);
		}
		
		else if(resource.component === 'FYI Site'  && test
		&& resource.assignable
		&& resource.media_type === 'HTML'
		&& resource.component_type === 'Website'
		&& resource.categorization === 'Interactive Content'){
			result.push(resource);
		}
		
		else if(resource.component === 'Stream to Start Videos'  && test
		&& resource.assignable 
		&& resource.media_type === 'Video'
		&& resource.component_type === 'Video'
		&& resource.categorization === 'Audio and Video'){
			result.push(resource);
		}
		
		else if(resource.component === 'myNotebook'  && test
		&& resource.assignable 
		&& resource.media_type === 'HTML'
		&& resource.component_type === 'Ancillary'
		&& resource.categorization === 'Study Aids and Workbooks'){
			result.push(resource);
		}
		
		else if(resource.component === 'myWriteSmart'  && test
		&& resource.assignable
		&& resource.media_type === 'HTML'
		&& resource.component_type === 'Interactive Activity'
		&& resource.categorization === 'Interactive Content'){
			result.push(resource);
		}
		
	});
	return result;
}

function journeysFilter(resources, gradeResOnly) {
	var result = [];
	resources.forEach(function (resource) {
		var test = true;
		
		if(gradeResOnly){
			if(resource.L2){
				test = false;
			}else{
				test = true;
			}
		}else{
			if(resource.L2){
				test = true;
			}else{
				test = false;
			}
		}
		
		resource = {
			HMH_ID : resource.HMH_ID,
			component : resource.component,
			assignable : resource.assignable,
			media_type : resource.media_type,
			component_type : resource.component_type,
			categorization : resource.categorization,
		}
		
		if(resource.component === 'Audio Hub'  && test
		&& resource.assignable 
		&& resource.media_type === 'Audio'
		&& resource.component_type === 'Ancillary'
		&& resource.categorization === 'Audio and Video'){
			result.push(resource);
		}
		
		else if(resource.component === 'Student Book'  && test
		&& resource.assignable 
		&& resource.media_type === 'HTML'
		&& resource.component_type === 'Key Student Resource'
		&& resource.categorization === 'Core Components'){
			result.push(resource);
		}
		
		else if(resource.component === 'Write-In Reader'  && test
		&& resource.assignable 
		&& resource.media_type === 'HTML'
		&& resource.component_type === 'Intervention Resource'
		&& resource.categorization === 'Intervention Components'){
			result.push(resource);
		}
		
		else if(resource.component === 'Literacy Centers'  && test
		&& resource.assignable 
		&& resource.media_type === 'HTML'
		&& resource.component_type === 'Ancillary'
		&& resource.categorization === 'Study Aids and Workbooks'){
			result.push(resource);
		}
		
		else if(resource.component === "Reader's Notebook"  && test
		&& resource.assignable 
		&& resource.media_type === 'HTML'
		&& resource.component_type === 'Ancillary'
		&& resource.categorization === 'Study Aids and Workbooks'){
			result.push(resource);
		}
		
		else if(resource.component === 'Common Core Writing Handbook'  && test
		&& resource.assignable 
		&& resource.media_type === 'HTML'
		&& resource.component_type === 'Ancillary'
		&& resource.categorization === 'Study Aids and Workbooks'){
			result.push(resource);
		}
		
		else if(resource.component === 'Vocabulary in Context Cards'  && test
		&& resource.assignable 
		&& resource.media_type === 'HTML'
		&& resource.component_type === 'Ancillary'
		&& resource.categorization === 'Study Aids and Workbooks'){
			result.push(resource);
		}
		
		else if(resource.component === 'Big Books'  && test
		&& resource.assignable 
		&& resource.media_type === 'HTML'
		&& resource.component_type === 'Ancillary'
		&& resource.categorization === 'Core Components'){
			result.push(resource);
		}
		
		else if(resource.component === 'Decodable Readers'  && test
		&& resource.assignable 
		&& resource.media_type === 'HTML'
		&& resource.component_type === 'Reader'
		&& resource.categorization === 'Reader'){
			result.push(resource);
		}
		
		else if(resource.component === 'Multimedia Grammar Glossary'  && test
		&& resource.assignable 
		&& resource.media_type === 'HTML'
		&& resource.component_type === 'Ancillary'
		&& resource.categorization === 'Interactive Content'){
			result.push(resource);
		}
		
		else if(resource.component === 'Channel One News'  && test
		&& resource.assignable 
		&& resource.media_type === 'Video'
		&& resource.component_type === 'Audio and Video'
		&& resource.categorization === 'Audio and Video'){
			result.push(resource);
		}
		
		else if(resource.component === 'FYI Site'  && test
		&& resource.assignable 
		&& resource.media_type === 'HTML'
		&& resource.component_type === 'Website'
		&& resource.categorization === 'Interactive Content'){
			result.push(resource);
		}
		
		else if(resource.component === 'HMH in the News'  && test
		&& resource.assignable 
		&& resource.media_type === 'HTML'
		&& resource.component_type === 'Website'
		&& resource.categorization === 'Interactive Content'){
			result.push(resource);
		}
		
		else if(resource.component === 'Stream to Start Videos'  && test
		&& resource.assignable 
		&& resource.media_type === 'Video'
		&& resource.component_type === 'Video'
		&& resource.categorization === 'Audio and Video'){
			result.push(resource);
		}
		
		else if(resource.component === 'myWriteSmart'  && test
		&& resource.assignable 
		&& resource.media_type === 'HTML'
		&& resource.component_type === 'Interactive Activity'
		&& resource.categorization === 'Interactive Content'){
			result.push(resource);
		}
		
	});
	return result;
}

module.exports.collectionFilter = collectionFilter;
module.exports.escalateFilter = escalateFilter;
