module.exports = function (input) {
    var outputArray = [];
    var traverse = function (input) {
        for (i in input) {
            if (!!input[i] && typeof (input[i]) == "object") {
                if (i == 'resource') {
                    for (var j = 0; j < input[i].length; j++) {
                        outputArray.push(input[i][j]);
                    }
                }
                traverse(input[i]);
            }
        }
    }
    traverse(input);
    // outputArray.forEach(function(value, index){
    //     console.log(value.id);
    // })
    
    return outputArray;
}
