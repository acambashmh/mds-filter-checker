function extractStandards(flatRow, item) {
    if (item.standards && item.standards[0].standard && item.standards[0].standard[0] !== '') {
        var standardsString = '';
        var standardsArray = item.standards[0].standard;

        for (var i = 0, len = standardsArray.length; i < len; i++) {

            standardsString += standardsArray[i];
            if (i + 1 !== len) {
                standardsString += ', ';
            }
        }
        flatRow.standards = standardsString;
    }
}

function convertNumber(number){
    if(!isNaN(number)){
        return parseInt(number);
    }
    return null;
}

function convertBoolean(bool){
    if(bool.toLowerCase() === 'true'){
        return true;
    }
    if(bool.toLowerCase() === 'false'){
        return false;
    }
    return null;
}


function createLs(flatRow, level) {
    if (level.level) {
        createLs(flatRow, level.level);
    }

    flatRow['L' + level.level_number] =  convertNumber(level.hierarchy);
    flatRow['L' + level.level_number + '_title'] = level.title ? level.title : '';
    flatRow['L' + level.level_number + '_type'] = level.type;
}

function handleCardValue(flatRow) {
    if (flatRow && flatRow.L7) {
        flatRow.card = flatRow.L7;
    }

    delete flatRow.L7;
    delete flatRow.L7_type;
    delete flatRow.L7_title;
}

function handleGradeLvlValue(flatRow, level) {
    if (flatRow.L0 && level.grades) {
        flatRow.L0 = level.grades.grade[0];
    }
}


function extractLevel(flatRow, level) {
    createLs(flatRow, level );
    handleCardValue(flatRow);
    handleGradeLvlValue(flatRow, level);
}



function convertResourceXMLItemToJson(item) {
    item.assignable = convertBoolean(item.assignable); 
    item.freeplay = convertBoolean(item.freeplay); 
    item.active = convertBoolean(item.active); 
    item.reteach = convertBoolean(item.reteach); 
    item.diff_inst = convertBoolean(item.diff_inst); 
    item.viewable = convertBoolean(item.viewable); 
    item.schedulable = convertBoolean(item.schedulable); 
    item.searchable = convertBoolean(item.searchable); 
    item.teacher_managed = convertBoolean(item.teacher_managed); 
    item.se_facing = convertBoolean(item.se_facing); 
    item.enrich = convertBoolean(item.enrich); 
    item.tool_type = convertNumber(item.tool_type);
    item.number = convertNumber(item.number);
    item.hierarchy = convertNumber(item.hierarchy);
    item.strand = convertNumber(item.strand);
    
    extractLevel(item, item.level);
    return item;
}


module.exports = convertResourceXMLItemToJson;